coffee = require 'coffee-script'
path = require('path').resolve(__dirname, '../bw/attachment.coffee')
eval(coffee.compile(require('fs').readFileSync(path, 'utf8'), bare: true))

delete @Status.Sleep::switchOut

# In XY, electric pokemon are immune to paralysis
@Status.Paralyze.worksOn = (battle, pokemon) ->
  !pokemon.hasType("Electric")

# In XY, Scald and Steam Eruption thaw the target
@Status.Freeze::afterBeingHit = (move, user, target) ->
  if !move.isNonDamaging() && move.type == 'Fire' ||
      move.name in ["Scald", "Steam Eruption"]
    @pokemon.cureStatus()

# In XY, Protect-like moves have a chance of success corresponding to the
# power of 3, instead of the power of 2 in previous generations.
@Attachment.ProtectCounter::successMultiplier = 3

# In XY, partial-trapping moves deal more damage at the end of each turn
@Attachment.Trap::getDamagePerTurn = ->
  if @user.hasItem("Binding Band")
    6
  else
    8

class @Attachment.KingsShield extends @VolatileAttachment
  name: "KingsShieldAttachment"

  initialize: ->
    @pokemon.tell(Protocol.POKEMON_ATTACH, @name)

  shouldBlockExecution: (move, user) ->
    if move.hasFlag("protect") && !move.isNonDamaging()
      if move.hasFlag("contact") then user.boost(attack: -2, @pokemon)
      return true

  endTurn: ->
    @pokemon.unattach(@constructor)

  unattach: ->
    @pokemon.tell(Protocol.POKEMON_UNATTACH, @name)

class @Attachment.SpikyShield extends @VolatileAttachment
  name: "SpikyShieldAttachment"

  initialize: ->
    @pokemon.tell(Protocol.POKEMON_ATTACH, @name)

  shouldBlockExecution: (move, user) ->
    if move.hasFlag("protect")
      if move.hasFlag("contact") then user.damage(user.stat('hp') >> 3)
      return true

  endTurn: ->
    @pokemon.unattach(@constructor)

  unattach: ->
    @pokemon.tell(Protocol.POKEMON_UNATTACH, @name)

class @Attachment.StickyWeb extends @TeamAttachment
  name: "StickyWebAttachment"

  initialize: ->
    id = @team.playerId
    @battle.cannedText('STICKY_WEB_START', @battle.getPlayerIndex(id))

  switchIn: (pokemon) ->
    if !pokemon.isImmune("Ground")
      @battle.cannedText('STICKY_WEB_CONTINUE', pokemon)
      # The source is not actually an opposing Pokemon, but in order for Defiant
      # to work properly, the source should not be the pokemon itself.
      pokemon.boost(speed: -1, @battle.getAllOpponents(pokemon)[0])

  unattach: ->
    id = @team.playerId
    @battle.cannedText('STICKY_WEB_END', @battle.getPlayerIndex(id))

class @Attachment.Livewire extends @TeamAttachment
  name: "LiveWireAttachment"

  maxLayers: 1

  initialize: ->
    id = @team.playerId
    @battle.cannedText('LIVEWIRE_START', @battle.getPlayerIndex(id))

  #On switch in
  switchIn: (pokemon) ->
    if pokemon.isFainted()
      return
    #if pokemon is part electric and not immune to ground, remove livewire. Also if has ground and is not immune to ground
    if (pokemon.hasType("Electric") && !pokemon.isImmune("Ground")) || (pokemon.hasType("Ground") && !pokemon.isImmune("Ground"))
      @team.unattach(@constructor)
      return

    #if pokemon is immune to electric, nothing happens
    if pokemon.isImmune("Electric")
      return
    #if pokemon is immune to ground, show miss message and nothing further happens
    if pokemon.isImmune("Ground")
      @battle.cannedText('LIVEWIRE_MISS', pokemon)
      return

    rng = @battle?.rng.randInt(1, 100, "will livewire paralyze?")
    livewirechance = 70
    if rng < livewirechance
      pokemon.cureStatus()
      pokemon.attach(Status.Paralyze)
      @battle.cannedText('LIVEWIRE_HURT', pokemon)
    else 
       @battle.cannedText('LIVEWIRE_MISS', pokemon)
    return

  unattach: ->
    id = @team.playerId
    @battle.cannedText('LIVEWIRE_END', @battle.getPlayerIndex(id))
    return

class @Attachment.FireRock extends @TeamAttachment
  name: "FireRockAttachment"

  initialize: ->
    id = @team.playerId
    @battle.cannedText('FIRE_ROCK_START', @battle.getPlayerIndex(id))

  switchIn: (pokemon) ->
    multiplier = util.typeEffectiveness("Fire", pokemon.types)
    hp = pokemon.stat('hp')
    damage = ((hp * multiplier) >> 3)
    if pokemon.damage(damage)
      @battle.cannedText('FIRE_ROCK_HURT', pokemon)

  unattach: ->
    id = @team.playerId
    @battle.cannedText('FIRE_ROCK_END', @battle.getPlayerIndex(id))

class @Attachment.Pendulum extends @VolatileAttachment
  name: "PendulumAttachment"

  maxLayers: 5

  initialize: (attributes) ->
    {@move} = attributes

  beforeMove: (move) ->
    @pokemon.unattach(@constructor)  if move != @move

# 1.2 attachments
class @Attachment.Permafrost extends @TeamAttachment
  name: "PermafrostAttachment"

  maxLayers: 1

  initialize: ->
    id = @team.playerId
    @battle.cannedText('PERMAFROST_START', @battle.getPlayerIndex(id))

  #On switch in
  switchIn: (pokemon) ->
    if pokemon.isFainted()
      return

    #if pokemon is immune to electric, nothing happens
    if pokemon.isImmune("Ice")
      return
    #if pokemon is immune to ground, show miss message and nothing further happens
    if pokemon.isImmune("Ground")
      @battle.cannedText('PERMAFROST_MISS', pokemon)
      return

    rng = @battle?.rng.randInt(1, 100, "will permafrost freeze?")
    permafrostchance = 13
    if rng < permafrostchance
      pokemon.cureStatus()
      pokemon.attach(Status.Frozen)
      @battle.cannedText('PERMAFROST_HURT', pokemon)
    else 
       @battle.cannedText('PERMAFROST_MISS', pokemon)

  unattach: ->
    id = @team.playerId
    @battle.cannedText('PERMAFROST_END', @battle.getPlayerIndex(id))

class @Attachment.Jetstream extends @TeamAttachment
  name: "JetstreamAttachment"

  initialize: ->
    id = @team.playerId
    @battle.cannedText('JETSTREAM_ADDED', @battle.getPlayerIndex(id))

  beforeMove: (move) ->
    move.priority += 1
    @battle.cannedText('JETSTREAM_TRIGGER', @pokemon)
    @team.unattach(@constructor)
