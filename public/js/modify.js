var element = document.getElementById('editor_holder');

var f = document.createElement("form");
f.setAttribute('method', "post");
f.setAttribute('action', "submit");

function adddata(type, value, row) {
    var i = document.createElement("input");
    i.setAttribute('type', "text");
    i.setAttribute('name', type);
    i.setAttribute('value', value);
    i.setAttribute("id", value)
    row.appendChild(i)
}

for (var key in pokedata) {
    if (!pokedata.hasOwnProperty(key)) continue;

    var table = document.createElement('table');
    f.appendChild(table)

    namerow = document.createElement('tr');
    namerow.setAttribute("class", "name")
    adddata("name", key, namerow)
    table.appendChild(namerow)

    var formerow = document.createElement('tr');
    formerow.setAttribute("class", "forme");
    table.appendChild(formerow)

    var obj = pokedata[key]
    i = 0
    for (var prop in obj) {
        if (!obj.hasOwnProperty(prop)) continue;

        var formecell = document.createElement('td');
        formecell.colSpan = '2';
        formerow.appendChild(formecell)

        adddata("forme", prop, formecell)
        
        i++
        var obj2 = obj[prop]
        console.log(obj2)
        abilities = obj2.abilities
        abilitystring = abilities.join(', ')
        abilityrow = document.createElement('tr');
        table.appendChild(abilityrow)
        _abilitycell = document.createElement('td');
        abilityrow.appendChild(_abilitycell)
        _abilitycell.innerHTML = "Abilities";
        abilitycell = document.createElement('td');
        abilityrow.appendChild(abilitycell)
        adddata("abilities", abilitystring, abilitycell)
    }

}




//and some more input elements here
//and dont forget to add a submit button

document.getElementsByTagName('body')[0].appendChild(f);
